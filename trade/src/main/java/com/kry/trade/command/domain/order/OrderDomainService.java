package com.kry.trade.command.domain.order;

import java.util.List;

public interface OrderDomainService {

    Receiver getDefaultReceiver(String customerId);

    void bookInventory(String orderId, List<OrderItem> items);
}
