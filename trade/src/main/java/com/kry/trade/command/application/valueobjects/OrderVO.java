package com.kry.trade.command.application.valueobjects;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.List;

@Accessors(chain = true)
@Getter
@Setter
public class OrderVO {

    private String orderId;

    private String customerId;
    private String customerName;

    private BigDecimal finalAmount;
    private BigDecimal originAmount;
    private BigDecimal discountAmount;

    private List<OrderItemVO> items;

    private String paymentMode;

    private ReceiverVO receiver;

}
