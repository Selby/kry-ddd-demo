package com.kry.trade.command.domain.order;

import com.kry.exception.UnrecognizedException;
import org.springframework.util.StringUtils;

public enum OrderStatus {

    CREATED("N"), WAITING_FOR_PAY("P"), WAITING_FOR_SHIPPING("S"), FINISHED("F"), CANCELED("C");

    private String status;

    OrderStatus(String status) {
        this.status = status;
    }

    public String value() {
        return status;
    }

    public static OrderStatus from(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        if (value.equals("N")) {
            return CREATED;
        } else if (value.equals("P")) {
            return WAITING_FOR_PAY;
        } else if (value.equals("S")) {
            return WAITING_FOR_SHIPPING;
        } else if (value.equals("F")) {
            return FINISHED;
        } else if (value.equals("C")) {
            return CANCELED;
        } else {
            throw new UnrecognizedException();
        }
    }
}
