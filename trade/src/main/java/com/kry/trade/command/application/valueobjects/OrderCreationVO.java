package com.kry.trade.command.application.valueobjects;

import com.google.common.collect.Lists;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Accessors(chain = true)
@Data
public class OrderCreationVO {
    @NotNull
    @NotEmpty
    private List<OrderItemCreationVO> items;

    public void addItem(OrderItemCreationVO itemCreationVO) {
        if (items == null) {
            items = Lists.newArrayList();
        }
        items.add(itemCreationVO);
    }

}