package com.kry.trade.command.domain.order;

import com.kry.domain.BaseDomainEvent;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OrderConfirmedEvent extends BaseDomainEvent {

    private String orderId;
    private String status;

}
