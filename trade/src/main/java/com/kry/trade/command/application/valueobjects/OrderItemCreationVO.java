package com.kry.trade.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class OrderItemCreationVO extends OrderItemBaseVO {
}
