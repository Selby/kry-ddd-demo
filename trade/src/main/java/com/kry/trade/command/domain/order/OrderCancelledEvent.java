package com.kry.trade.command.domain.order;

import com.kry.domain.BaseDomainEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class OrderCancelledEvent extends BaseDomainEvent {

    private String orderId;

}
