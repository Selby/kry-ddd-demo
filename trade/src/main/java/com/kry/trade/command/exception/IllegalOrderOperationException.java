package com.kry.trade.command.exception;

import com.kry.exception.BusinessException;

public class IllegalOrderOperationException extends BusinessException {

    public IllegalOrderOperationException(String errorMessage) {
        super(ErrorCode.ILLEGAL_ORDER_OPERATION, errorMessage);
    }
}
