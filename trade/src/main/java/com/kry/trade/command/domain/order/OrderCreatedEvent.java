package com.kry.trade.command.domain.order;

import com.kry.domain.BaseDomainEvent;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class OrderCreatedEvent extends BaseDomainEvent {

    private String orderId;

}
