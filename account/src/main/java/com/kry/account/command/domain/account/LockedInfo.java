package com.kry.account.command.domain.account;

import com.kry.domain.ValueObject;
import lombok.Builder;
import lombok.Getter;
import org.joda.time.DateTime;

import java.util.Date;

@Builder
@Getter
public class LockedInfo implements ValueObject {

    private Date lockedAt;
    private Date unlockAt;
    private String lockedReason;

    private static int LOCK_MINUTES = 30;

    public static LockedInfo lock() {
        DateTime date = DateTime.now();
        return LockedInfo.builder().lockedAt(date.toDate()).unlockAt(date.plusMinutes(LOCK_MINUTES).toDate()).build();
    }

}
