package com.kry.account.command.infra.repository.storageobjects;

import com.kry.account.command.infra.repository.mybatis.mapper.AccountMapper;
import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;

import java.io.Serializable;
import java.util.Date;

@StorageMapper(AccountMapper.class)
public class AccountSO extends BaseSO implements Serializable {

    private String accountId;

    private String accountName;

    private String password;

    private int status;

    private Date loginAt;

    private Date lockedAt;

    private Date unlockAt;

    private String lockedReason;

    private int timesOfLoginFailed;

    private static final long serialVersionUID = 1L;

    public String getAccountId() {
        return accountId;
    }

    public AccountSO withAccountId(String accountId) {
        this.setAccountId(accountId);
        return this;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public AccountSO withAccountName(String accountName) {
        this.setAccountName(accountName);
        return this;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPassword() {
        return password;
    }

    public AccountSO withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLoginAt() {
        return loginAt;
    }

    public AccountSO withLoginAt(Date loginAt) {
        this.setLoginAt(loginAt);
        return this;
    }

    public void setLoginAt(Date loginAt) {
        this.loginAt = loginAt;
    }

    public Date getLockedAt() {
        return lockedAt;
    }

    public AccountSO withLockedAt(Date lockedAt) {
        this.setLockedAt(lockedAt);
        return this;
    }

    public void setUnLockAt(Date unlockAt) {
        this.unlockAt = unlockAt;
    }


    public Date getUnLockedAt() {
        return unlockAt;
    }

    public AccountSO withUnLockedAt(Date unlockAt) {
        this.setUnLockAt(unlockAt);
        return this;
    }

    public void setLockedAt(Date lockedAt) {
        this.lockedAt = lockedAt;
    }


    public String getLockedReason() {
        return lockedReason;
    }

    public AccountSO withLockedReason(String lockedReason) {
        this.setLockedReason(lockedReason);
        return this;
    }

    public void setLockedReason(String lockedReason) {
        this.lockedReason = lockedReason;
    }

    public int getStatus() {
        return status;
    }

    public AccountSO withStatus(int status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public int getTimesOfLoginFailed() {
        return timesOfLoginFailed;
    }

    public AccountSO withTimesOfLoginFailed(int timesOfLoginFailed) {
        this.setTimesOfLoginFailed(timesOfLoginFailed);
        return this;
    }

    public void setTimesOfLoginFailed(int timesOfLoginFailed) {
        this.timesOfLoginFailed = timesOfLoginFailed;
    }


    @Override
    public Serializable getPrimaryKeyValue() {
        return accountId;
    }

}