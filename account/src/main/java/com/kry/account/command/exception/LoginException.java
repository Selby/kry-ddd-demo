package com.kry.account.command.exception;

import com.kry.exception.BusinessException;

public class LoginException extends BusinessException {

    public LoginException(String errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    public static LoginException newWithLocked() {
        return new LoginException(ErrorCode.ACCOUNT_LOCKED, "Account was locked, please try it again after 30 minutes.");
    }

    public static LoginException newWithInactive() {
        return new LoginException(ErrorCode.ACCOUNT_INACTIVE, "Account is inactive, please activate it first.");
    }

    public static LoginException newWithDisabled() {
        return new LoginException(ErrorCode.ACCOUNT_DISABLED, "Account was disabled and cannot login again. ");
    }
}
