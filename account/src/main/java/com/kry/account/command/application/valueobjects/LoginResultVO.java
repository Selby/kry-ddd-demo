package com.kry.account.command.application.valueobjects;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Setter
@Getter
public class LoginResultVO {

    private boolean logged;
    private String accountId;
    private String accountName;

}
