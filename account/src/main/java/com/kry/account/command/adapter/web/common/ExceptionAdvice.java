package com.kry.account.command.adapter.web.common;

import com.kry.adapter.web.Response;
import com.kry.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@ControllerAdvice
public class ExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Response wrapException(Exception e) {
        Response response;
        if (e instanceof BusinessException) {
            BusinessException exception = (BusinessException)e;
            response = Response.newWithFailed(exception.getErrorCode(), exception.getErrorMessage());
        } else {
            response = Response.newWithFailedByDefault();
            log.error(e.getMessage(), e);
        }
        return response;
    }

}
