package com.kry.account.command.domain.account;

import com.kry.account.command.domain.account.Account;
import com.kry.domain.Repository;

public interface AccountRepository extends Repository<Account> {

    Account loadByAccountName(String accountName);

}
