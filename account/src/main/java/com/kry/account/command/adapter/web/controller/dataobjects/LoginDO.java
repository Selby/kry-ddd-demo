package com.kry.account.command.adapter.web.controller.dataobjects;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginDO {
    @ApiModelProperty(value = "Account name", required = true)
    private String accountName;
    @ApiModelProperty(value = "Account password", required = true)
    private String password;
}
