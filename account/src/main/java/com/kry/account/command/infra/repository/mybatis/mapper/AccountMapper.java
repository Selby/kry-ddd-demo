package com.kry.account.command.infra.repository.mybatis.mapper;

import com.kry.account.command.infra.repository.storageobjects.AccountSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

@Mapper
public interface AccountMapper {

    @Insert({
            "insert into account (account_id, account_name, ",
            "pwd, status, login_at, ",
            "locked_at, unlock_at, locked_reason, ",
            "times_of_login_failed, create_user, ",
            "create_date, update_user, ",
            "update_date, ver)",
            "values (#{accountId,jdbcType=VARCHAR}, #{accountName,jdbcType=VARCHAR}, ",
            "#{password,jdbcType=VARCHAR}, #{status,jdbcType=TINYINT}, #{loginAt,jdbcType=TIMESTAMP}, ",
            "#{lockedAt,jdbcType=TIMESTAMP}, #{unlockAt,jdbcType=TIMESTAMP}, #{lockedReason,jdbcType=VARCHAR}, ",
            "#{timesOfLoginFailed,jdbcType=TINYINT}, #{createUser,jdbcType=VARCHAR}, ",
            "#{createDate,jdbcType=TIMESTAMP}, #{updateUser,jdbcType=VARCHAR}, ",
            "#{updateDate,jdbcType=TIMESTAMP}, #{ver,jdbcType=TINYINT})"
    })
    int insert(AccountSO record);


    @Select({
            "select",
            "account_id, account_name, pwd, status, login_at, locked_at, unlock_at, locked_reason, times_of_login_failed, ",
            "create_user, create_date, update_user, update_date, ver",
            "from account",
            "where account_id = #{accountId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "account_id", property = "accountId", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "account_name", property = "accountName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "pwd", property = "password", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.TINYINT),
            @Result(column = "login_at", property = "loginAt", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "locked_at", property = "lockedAt", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "unlock_at", property = "unlockAt", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "locked_reason", property = "lockedReason", jdbcType = JdbcType.VARCHAR),
            @Result(column = "times_of_login_failed", property = "timesOfLoginFailed", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_user", property = "createUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_user", property = "updateUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "ver", property = "ver", jdbcType = JdbcType.TINYINT)

    })
    AccountSO selectByPrimaryKey(String accountId);

    @Update({
            "update account",
            "set account_name = #{entity.accountName,jdbcType=VARCHAR},",
            "pwd = #{entity.password,jdbcType=VARCHAR},",
            "status = #{entity.status,jdbcType=TINYINT},",
            "login_at = #{entity.loginAt,jdbcType=TIMESTAMP},",
            "locked_at = #{entity.lockedAt,jdbcType=TIMESTAMP},",
            "unlock_at = #{entity.unlockAt,jdbcType=TIMESTAMP},",
            "locked_reason = #{entity.lockedReason,jdbcType=VARCHAR},",
            "times_of_login_failed = #{entity.timesOfLoginFailed,jdbcType=TINYINT},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where account_id = #{entity.accountId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);

    @Select({
            "select",
            "account_id, account_name, pwd, status, login_at, locked_at, unlock_at, locked_reason, times_of_login_failed, ",
            "create_user, create_date, update_user, update_date, ver",
            "from account",
            "where account_name = #{accountName,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "account_id", property = "accountId", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "account_name", property = "accountName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "pwd", property = "password", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.TINYINT),
            @Result(column = "login_at", property = "loginAt", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "locked_at", property = "lockedAt", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "unlock_at", property = "unlockAt", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "locked_reason", property = "lockedReason", jdbcType = JdbcType.VARCHAR),
            @Result(column = "times_of_login_failed", property = "timesOfLoginFailed", jdbcType = JdbcType.TINYINT),
            @Result(column = "create_user", property = "createUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_date", property = "createDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_user", property = "updateUser", jdbcType = JdbcType.VARCHAR),
            @Result(column = "update_date", property = "updateDate", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "ver", property = "ver", jdbcType = JdbcType.TINYINT)
    })
    AccountSO selectByAccountName(String accountName);

}