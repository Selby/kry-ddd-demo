package com.kry.account.command.exception;

public final class ErrorCode {

    public static final String ACCOUNT_LOCKED = "10001";

    public static final String ACCOUNT_INACTIVE = "10002";

    public static final String ACCOUNT_DISABLED = "10003";

    public static final String DUPLIDATED_ACCOUNT = "10004";
}
