package com.kry.account.command.exception;

import com.kry.exception.BusinessException;

public class DuplicateAccountException extends BusinessException {

    public DuplicateAccountException(String message) {
        super(ErrorCode.DUPLIDATED_ACCOUNT, message);
    }

}
