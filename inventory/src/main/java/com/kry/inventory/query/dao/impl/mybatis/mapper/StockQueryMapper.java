package com.kry.inventory.query.dao.impl.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

@Mapper
public interface StockQueryMapper {

    @Select({
            "select source_id ",
            "from stock_booking",
            "where expire_date < #{currentDate,jdbcType=TIMESTAMP} and status = 1"
    })
    List<String> findAllExpiredBooking(Date currentDate);
}
