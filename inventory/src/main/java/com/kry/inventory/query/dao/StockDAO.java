package com.kry.inventory.query.dao;

import java.util.List;

public interface StockDAO {

    List<String> findAllExpiredBooking();

}
