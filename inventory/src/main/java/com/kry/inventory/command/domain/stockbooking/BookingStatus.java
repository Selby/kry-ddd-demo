package com.kry.inventory.command.domain.stockbooking;

import com.kry.exception.UnrecognizedException;

public enum BookingStatus {

    BOOKED(1), EXPIRED(2), COMPLETED(3);

    private int status;

    BookingStatus(int status) {
        this.status = status;
    }

    public int value() {
        return status;
    }

    public static BookingStatus from(int value) {
        if (value == 1) {
            return BOOKED;
        } else if (value == 2) {
            return EXPIRED;
        } else if (value == 3) {
            return COMPLETED;
        } else {
            throw new UnrecognizedException();
        }
    }

}
