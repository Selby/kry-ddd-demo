package com.kry.inventory.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@Data
public class BookingExpiredVO {

    @NotNull
    private String stockId;
    @NotNull
    private int quantity;

}
