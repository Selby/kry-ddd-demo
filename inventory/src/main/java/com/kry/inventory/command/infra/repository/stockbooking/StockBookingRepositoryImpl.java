package com.kry.inventory.command.infra.repository.stockbooking;

import com.google.common.collect.Lists;
import com.kry.domain.AbstractRepository;
import com.kry.domain.DomainEvent;
import com.kry.exception.AggregateNotFoundException;
import com.kry.infra.repository.DaoProvider;
import com.kry.inventory.command.domain.stock.ProductId;
import com.kry.inventory.command.domain.stockbooking.BookingDetail;
import com.kry.inventory.command.domain.stockbooking.BookingStatus;
import com.kry.inventory.command.domain.stockbooking.StockBooking;
import com.kry.inventory.command.domain.stockbooking.StockBookingRepository;
import com.kry.inventory.command.infra.repository.stockbooking.mybatis.mapper.StockBookingMapper;
import com.kry.inventory.command.infra.repository.stockbooking.storageobjects.StockBookingSO;
import com.kry.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;

@Repository
public class StockBookingRepositoryImpl extends AbstractRepository<StockBooking> implements StockBookingRepository {

    @Autowired
    private DaoProvider daoProvider;
    @Autowired
    private StockBookingMapper bookingMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;


    @Override
    protected void saveAggregate(StockBooking aggregate) {
        StockBookingSO bookingSO = new StockBookingSO();
        bookingSO.setStockId(aggregate.getStockId())
                .setExpireDate(aggregate.getExpireDate()).setStatus(aggregate.getStatus().value())
                .setQuantity(aggregate.getBooking().getQuantity())
                .setSourceId(aggregate.getBooking().getSourceId())
                .setProductId(aggregate.getBooking().getProductId().getId());
        daoProvider.save(bookingSO);
    }

    @Override
    protected void publishEvent(StockBooking aggregate) {
        DomainEvent event = getEvent(aggregate);
        if (event != null) {
            kafkaTemplate.send(getDefaultTopicName(event), aggregate.getEntityId().toString(), JsonUtils.toJson(event));
        }
    }

    @Override
    public StockBooking load(Serializable aggregateId) {
        StockBookingSO bookingSO = daoProvider.loadOne(StockBookingSO.class, aggregateId);
        if (bookingSO == null) {
            throw new AggregateNotFoundException();
        }

        return buildStockBooking(bookingSO);
    }

    private StockBooking buildStockBooking(StockBookingSO bookingSO) {
        return StockBooking.builder().stockId(bookingSO.getStockId())
                .expireDate(bookingSO.getExpireDate()).status(BookingStatus.from(bookingSO.getStatus()))
                .booking(BookingDetail.builder().quantity(bookingSO.getQuantity())
                        .productId(new ProductId(bookingSO.getProductId()))
                        .sourceId(bookingSO.getSourceId()).build())
                .build();
    }

    @Override
    public StockBooking loadWith(String sourceId, String productId) {
        StockBookingSO bookingSO = bookingMapper.selectWith(sourceId, productId);
        StockBooking booking = null;
        if (bookingSO != null) {
            booking = buildStockBooking(bookingSO);
        }
        return booking;
    }

    @Override
    public List<StockBooking> loadBySource(String sourceId) {
        List<StockBookingSO> list = bookingMapper.selectBySource(sourceId);
        if (CollectionUtils.isEmpty(list)) {
            throw new AggregateNotFoundException();
        }
        List<StockBooking> result = Lists.newArrayList();
        for (StockBookingSO bookingSO: list) {
            result.add(buildStockBooking(bookingSO));
        }
        return result;
    }
}
