package com.kry.inventory.command.domain.stockbooking;

import com.kry.domain.Repository;

import java.util.List;

public interface StockBookingRepository extends Repository<StockBooking> {

    /**
     * If there is no booking found, the result will be null.
     * @param sourceId
     * @param productId
     * @return
     */
    StockBooking loadWith(String sourceId, String productId);

    /**
     * Load all stock bookings according to the ones who launched such as sales order.
     * @param sourceId
     * @return
     */
    List<StockBooking> loadBySource(String sourceId);

}
