package com.kry.inventory.command.infra.repository.stockchange;

import com.kry.domain.AbstractRepository;
import com.kry.inventory.command.domain.stock.Stock;
import com.kry.inventory.command.domain.stock.StockRepository;
import com.kry.inventory.command.domain.stockchange.StockChange;
import com.kry.inventory.command.domain.stockchange.StockChangeRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public class StockChangeRepositoryImpl extends AbstractRepository<StockChange> implements StockChangeRepository {


    @Override
    protected void saveAggregate(StockChange aggregate) {

    }

    @Override
    protected void publishEvent(StockChange aggregate) {

    }

    @Override
    public StockChange load(Serializable aggregateId) {
        return null;
    }
}
