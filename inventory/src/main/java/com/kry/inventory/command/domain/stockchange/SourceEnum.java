package com.kry.inventory.command.domain.stockchange;

import com.kry.exception.UnrecognizedException;
import org.springframework.util.StringUtils;

public enum SourceEnum {

    SALES_ORDER("SO"), SALES_CHARGE_BACK("SCB"), SCRAP("S"), CHECK("C");

    private String source;

    SourceEnum(String source) {
        this.source = source;
    }

    public String value() {
        return source;
    }

    public static SourceEnum from(String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        if (value.equals("SO")) {
            return SALES_ORDER;
        } else if (value.equals("SCB")) {
            return SALES_CHARGE_BACK;
        } else if (value.equals("S")) {
            return SCRAP;
        } else if (value.equals("C")) {
            return CHECK;
        } else {
            throw new UnrecognizedException();
        }
    }
}
