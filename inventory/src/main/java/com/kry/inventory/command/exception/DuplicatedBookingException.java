package com.kry.inventory.command.exception;

import com.kry.exception.BusinessException;

public class DuplicatedBookingException extends BusinessException {

    public DuplicatedBookingException() {
        super(ErrorCode.STOCK_BOOKING_DUPLICATED);
    }

}
