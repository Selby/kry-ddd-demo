package com.kry.inventory.command.domain.stockbooking;

import com.kry.domain.Aggregate;
import com.kry.inventory.command.exception.InvalidBookingStatusException;
import com.kry.utils.KryAssert;
import lombok.Builder;
import lombok.Getter;
import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Builder
@Getter
public class StockBooking extends Aggregate {

    // TODO Should be configured at Config Center.
    private static final int LOCK_MINUTES = 15;

    private String stockId;
    private Date expireDate;
    private BookingStatus status;

    private BookingDetail booking;


    public void create() {
        KryAssert.notNull(booking, "booking must be built when building stock booking.");
        stockId = UUID.randomUUID().toString();
        status = BookingStatus.BOOKED;
        expireDate = DateTime.now().plusMinutes(LOCK_MINUTES).toDate();
    }

    public void expire() {
        checkIfBookedStatus();
        // TODO Check if related business is completed. Only when not completed expiration can be done.
        status = BookingStatus.EXPIRED;
        applyEvent(new BookingExpiredEvent(booking.getProductId().getId(), booking.getQuantity()));
    }

    private void checkIfBookedStatus() {
        if (status != BookingStatus.BOOKED) {
            throw new InvalidBookingStatusException(String.format("Expected %s, actually %s.",
                    BookingStatus.BOOKED, status));
        }
    }

    public void complete() {
        checkIfBookedStatus();
        status = BookingStatus.COMPLETED;
        applyEvent(new BookingCompletedEvent(booking.getProductId().getId(), booking.getQuantity()));
    }


    @Override
    public Serializable getAggregateId() {
        return stockId;
    }
}
