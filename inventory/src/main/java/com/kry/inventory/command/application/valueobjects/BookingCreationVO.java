package com.kry.inventory.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@Data
public class BookingCreationVO {

    @NotNull
    private String sourceId;
    @NotNull
    private String productId;
    @NotNull
    private int quantity;

}
