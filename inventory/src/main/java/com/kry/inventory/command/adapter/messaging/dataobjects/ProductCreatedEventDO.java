package com.kry.inventory.command.adapter.messaging.dataobjects;

import lombok.Data;

@Data
public class ProductCreatedEventDO {

    private String productId;
    private String productName;

}
