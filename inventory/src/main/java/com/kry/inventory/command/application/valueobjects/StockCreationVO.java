package com.kry.inventory.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Accessors(chain = true)
@Data
public class StockCreationVO {

    @NotNull
    private String productId;
    @NotNull
    private String productName;
    @Min(0)
    private int quantity;

}
