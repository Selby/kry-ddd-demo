package com.kry.inventory.command.domain.stockbooking;

import com.kry.domain.ValueObject;
import lombok.Getter;

@Getter
public class StockId implements ValueObject {

    private String id;

    public StockId(String id) {
        this.id = id;
    }

}
