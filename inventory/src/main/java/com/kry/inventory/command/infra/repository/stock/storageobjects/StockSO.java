package com.kry.inventory.command.infra.repository.stock.storageobjects;

import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.inventory.command.infra.repository.stock.mybatis.mapper.StockMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Accessors(chain = true)
@Data
@StorageMapper(StockMapper.class)
public class StockSO extends BaseSO {

    private String stockId;
    private String productName;

    private int stockCnt;
    private int bookedCnt;
    private int lockedCnt;
    private int availableCnt;


    @Override
    public Serializable getPrimaryKeyValue() {
        return stockId;
    }

}
