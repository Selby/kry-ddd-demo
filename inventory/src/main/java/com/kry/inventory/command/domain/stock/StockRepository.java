package com.kry.inventory.command.domain.stock;

import com.kry.domain.Repository;

public interface StockRepository extends Repository<Stock> {
}
