package com.kry.inventory.command.adapter.messaging.dataobjects;

import lombok.Data;

@Data
public class BookingExpiredEventDO {

    private String stockId;
    private int quantity;

}
