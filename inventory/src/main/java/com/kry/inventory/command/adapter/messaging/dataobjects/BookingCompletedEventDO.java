package com.kry.inventory.command.adapter.messaging.dataobjects;

import lombok.Data;

@Data
public class BookingCompletedEventDO {

    private String stockId;
    private int quantity;

}
