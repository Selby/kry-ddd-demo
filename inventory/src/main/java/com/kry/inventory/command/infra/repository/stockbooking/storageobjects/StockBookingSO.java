package com.kry.inventory.command.infra.repository.stockbooking.storageobjects;

import com.kry.infra.repository.BaseSO;
import com.kry.infra.repository.mybatis.annotation.StorageMapper;
import com.kry.inventory.command.infra.repository.stockbooking.mybatis.mapper.StockBookingMapper;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Accessors(chain = true)
@Data
@StorageMapper(StockBookingMapper.class)
public class StockBookingSO extends BaseSO {

    private String stockId;
    private String sourceId;
    private String productId;
    private int quantity;
    private Date expireDate;
    private int status;

    @Override
    public Serializable getPrimaryKeyValue() {
        return stockId;
    }
}
