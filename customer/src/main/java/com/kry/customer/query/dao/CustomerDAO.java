package com.kry.customer.query.dao;

import com.kry.customer.query.dataobjects.ReceiverDO;

import java.util.List;

public interface CustomerDAO {

    List<ReceiverDO> findAllReceivers(String customerId);

}
