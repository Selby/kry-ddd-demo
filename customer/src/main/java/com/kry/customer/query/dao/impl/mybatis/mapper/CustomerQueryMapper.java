package com.kry.customer.query.dao.impl.mybatis.mapper;

import com.kry.customer.query.dataobjects.ReceiverDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface CustomerQueryMapper {

    @Select({
            "select",
            "receiver_id, name, mobile, province, city, district, street, building_and_number ",
            "from receiver",
            "where customer_id = #{customerId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="receiver_id", property="receiverId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="name", property="receiverName", jdbcType=JdbcType.VARCHAR),
            @Result(column="mobile", property="receiverMobile", jdbcType=JdbcType.VARCHAR),
            @Result(column="province", property="province", jdbcType=JdbcType.VARCHAR),
            @Result(column="city", property="city", jdbcType=JdbcType.VARCHAR),
            @Result(column="district", property="district", jdbcType=JdbcType.VARCHAR),
            @Result(column="street", property="street", jdbcType=JdbcType.VARCHAR),
            @Result(column="building_and_number", property="buildingAndNumber", jdbcType=JdbcType.VARCHAR)
    })
    List<ReceiverDO> getAllReceivers(String receiverId);

}
