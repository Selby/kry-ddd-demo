package com.kry.customer.query.dataobjects;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class ReceiverDO {

    private String receiverId;

    private String receiverName;
    private String receiverMobile;

    private String province;
    private String city;
    private String district;
    private String street;
    private String buildingAndNumber;

}
