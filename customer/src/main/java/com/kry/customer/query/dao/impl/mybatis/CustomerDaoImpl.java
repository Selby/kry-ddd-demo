package com.kry.customer.query.dao.impl.mybatis;

import com.kry.customer.query.dataobjects.ReceiverDO;
import com.kry.customer.query.dao.CustomerDAO;
import com.kry.customer.query.dao.impl.mybatis.mapper.CustomerQueryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerDaoImpl implements CustomerDAO {

    @Autowired
    private CustomerQueryMapper receiverMapper;

    @Override
    public List<ReceiverDO> findAllReceivers(String customerId) {
        return receiverMapper.getAllReceivers(customerId);
    }

}
