package com.kry.customer.command.adapter.web.controller;

import com.kry.customer.command.application.service.CustomerApplicationService;
import com.kry.customer.command.application.valueobjects.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Manage customer information", tags = "100 - Customer APIs")
@RestController
@RequestMapping("/customer")

public class CustomerController {

    @Autowired
    private CustomerApplicationService customerService;


    @ApiOperation(value = "Load customer info according customer id.", notes = "Load customer info according customer id.")
    @ResponseBody
    @GetMapping(value = "/{customerId}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public CustomerVO loadCustomer(@PathVariable String customerId) {
        return customerService.loadCustomer(customerId);
    }


    @ApiOperation(value = "Change customer basic information.", notes = "Change customer basic information.")
    @ResponseBody
    @PatchMapping(value = "/{customerId}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changeBasicInfo(@PathVariable String customerId, @RequestBody CustomerBasicInfoVO basicInfo) {
        customerService.changeBasicInfo(customerId, basicInfo);
    }


    @ApiOperation(value = "Change mobile No.", notes = "Change mobile No.")
    @ResponseBody
    @PatchMapping(value = "/{customerId}/{mobileNo}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changeMobile(@PathVariable String customerId, @PathVariable String mobileNo) {
        customerService.changeMobile(customerId, mobileNo);
    }


    @ApiOperation(value = "New a receiver for customer.", notes = "New a receiver for customer.")
    @ResponseBody
    @PostMapping(value = "/{customerId}/receiver",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void createReceiver(@PathVariable String customerId, @RequestBody @Validated ReceiverCreationVO receiver) {
        customerService.createReceiver(customerId, receiver);
    }

    @ApiOperation(value = "Change receiver info for customer.", notes = "Change receiver info for customer.")
    @ResponseBody
    @PatchMapping(value = "/{customerId}/receiver/{receiverId}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void changeReceiver(@PathVariable String customerId, @PathVariable String receiverId,
                               @RequestBody @Validated ReceiverChangeVO receiver) {
        customerService.changeReceiverInfo(customerId, receiverId, receiver);
    }

    @ApiOperation(value = "Find receivers of customer.", notes = "Find receivers of customer.")
    @ResponseBody
    @GetMapping(value = "/{customerId}/receivers",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ReceiverVO> loadReceiver(@PathVariable String customerId) {
        return customerService.loadReceivers(customerId);
    }

    @ApiOperation(value = "Set receiver as default receiver.", notes = "Set receiver as default receiver.")
    @ResponseBody
    @PatchMapping(value = "/{customerId}/receiver/{receiverId}/default",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void setAsDefaultReceiver(@PathVariable String customerId, @PathVariable String receiverId) {
        customerService.setDefaultReceiver(customerId, receiverId);
    }


    @ApiOperation(value = "Get default receiver.", notes = "Get default receiver.")
    @ResponseBody
    @GetMapping(value = "/{customerId}/receiver/default",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ReceiverVO getDefaultReceiver(@PathVariable String customerId) {
        return customerService.getDefaultReceiver(customerId);
    }

}
