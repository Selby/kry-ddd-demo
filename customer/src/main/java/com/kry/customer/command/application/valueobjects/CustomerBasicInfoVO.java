package com.kry.customer.command.application.valueobjects;

import lombok.Data;

import java.util.Date;

@Data
public class CustomerBasicInfoVO {

    private String realName;
    private Date birthday;
    private String gender;

}
