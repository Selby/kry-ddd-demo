package com.kry.customer.command.domain.customer;

import com.kry.domain.ValueObject;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Address implements ValueObject {

    private String province;
    private String city;
    private String district;
    private String street;
    private String buildingAndNumber;

}
