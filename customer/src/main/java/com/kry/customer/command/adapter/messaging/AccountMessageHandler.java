package com.kry.customer.command.adapter.messaging;

import com.kry.customer.command.adapter.messaging.dataobjects.AccountRegisteredEventDO;
import com.kry.customer.command.application.service.CustomerApplicationService;
import com.kry.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AccountMessageHandler {

    @Autowired
    private CustomerApplicationService customerService;

    @KafkaListener(topics = "Account.AccountRegisteredEvent")
    public void handleRegisteredEvent(Consumer<?,?> consumer, ConsumerRecord<String, String> record) {
        try {
            AccountRegisteredEventDO eventVO = JsonUtils.fromJson(AccountRegisteredEventDO.class, record.value());
            customerService.createCustomer(eventVO.getAccountId());
            consumer.commitAsync();
        } catch (Exception e) {
            log.error("Failed to consumer account registered event",e);
        }
    }

}
