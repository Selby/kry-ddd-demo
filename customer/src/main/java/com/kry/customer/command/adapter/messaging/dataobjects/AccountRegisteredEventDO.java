package com.kry.customer.command.adapter.messaging.dataobjects;

import lombok.Data;

@Data
public class AccountRegisteredEventDO {

    private String accountId;
    private String accountName;

}
