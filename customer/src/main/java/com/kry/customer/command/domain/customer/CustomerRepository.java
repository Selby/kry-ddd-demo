package com.kry.customer.command.domain.customer;

import com.kry.domain.Repository;

public interface CustomerRepository extends Repository<Customer> {

}
