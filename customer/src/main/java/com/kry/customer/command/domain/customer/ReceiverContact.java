package com.kry.customer.command.domain.customer;

import com.kry.domain.ValueObject;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ReceiverContact implements ValueObject {

    private String mobile;

}
