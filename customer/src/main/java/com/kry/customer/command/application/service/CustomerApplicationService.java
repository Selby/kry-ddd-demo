package com.kry.customer.command.application.service;

import com.google.common.collect.Lists;
import com.kry.customer.command.application.valueobjects.*;
import com.kry.customer.command.domain.customer.Customer;
import com.kry.customer.command.domain.customer.CustomerRepository;
import com.kry.customer.command.domain.customer.Receiver;
import com.kry.exception.InvalidParameterException;
import com.kry.utils.KryAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Transactional
@Service
public class CustomerApplicationService {

    @Autowired
    private CustomerRepository customerRepository;


    public void createCustomer(String accountId) {
        if (StringUtils.isEmpty(accountId)) {
            throw new InvalidParameterException("accountId must be provided.");
        }
        Customer customer = Customer.builder().build();
        customer.createCustomerWithAccount(accountId);

        customerRepository.save(customer);
    }

    @Transactional(readOnly = true)
    public CustomerVO loadCustomer(String customerId) {
        if (StringUtils.isEmpty(customerId)) {
            throw new InvalidParameterException("customerId must be provided.");
        }

        Customer customer = customerRepository.load(customerId);

        CustomerVO customerVO = new CustomerVO();
        customerVO.setBirthday(customer.getBirthday()).setCustomerId(customer.getId())
                .setEmail(customer.getContact().getEmail()).setMobile(customer.getContact().getMobile())
                .setGender(customer.getGender() != null ? customer.getGender().toString() : null)
                .setRealName(customer.getRealName());

        return customerVO;
    }


    public void changeBasicInfo(String customerId, CustomerBasicInfoVO basicInfoVO) {
        if (StringUtils.isEmpty(customerId)) {
            throw new InvalidParameterException("customerId must be provided.");
        }
        if (basicInfoVO == null) {
            throw new InvalidParameterException("basicInfoVO must be provided.");
        }

        Customer customer = customerRepository.load(customerId);
        customer.changeBasicInfo(basicInfoVO);

        customerRepository.save(customer);
    }

    public void changeMobile(String customerId, String mobile) {
        if (StringUtils.isEmpty(customerId)) {
            throw new InvalidParameterException("customerId must be provided.");
        }
        if (StringUtils.isEmpty(mobile)) {
            throw new InvalidParameterException("mobile must be provided.");
        }

        Customer customer = customerRepository.load(customerId);
        customer.changeMobile(mobile);

        customerRepository.save(customer);
    }

    public void createReceiver(String customerId, @Validated ReceiverCreationVO receiverVO) {
        KryAssert.notNull(receiverVO, "receiverVO must be provided");
        KryAssert.notEmpty(customerId, "customerId must be provided.");

        Customer customer = customerRepository.load(customerId);
        customer.addReceiver(receiverVO);

        customerRepository.save(customer);
    }

    public void changeReceiverInfo(String customerId, String receiverId, ReceiverChangeVO receiverVO) {
        KryAssert.notNull(receiverVO, "receiverVO must be provided");
        KryAssert.notEmpty(receiverId, "receiverId must be provided.");
        KryAssert.notEmpty(customerId, "customerId must be provided.");

        Customer customer = customerRepository.load(customerId);
        customer.changeReceiver(receiverId, receiverVO);

        customerRepository.save(customer);
    }

    public List<ReceiverVO> loadReceivers(String customerId) {
        KryAssert.notEmpty(customerId, "customerId must be provided.");

        Customer customer = customerRepository.load(customerId);

        List<ReceiverVO> result = Lists.newArrayList();

        if (!CollectionUtils.isEmpty(customer.getReceivers())) {
            customer.getReceivers().stream().forEach( receiver -> {
                result.add(populateReceiverVO(receiver));
            });
        }

        return result;
    }

    public void setDefaultReceiver(String customerId, String receiverId) {
        KryAssert.notEmpty(customerId, "customerId must be provided.");
        KryAssert.notEmpty(receiverId, "receiverId must be provided.");

        Customer customer = customerRepository.load(customerId);
        customer.setDefaultReceiver(receiverId);

        customerRepository.save(customer);
    }

    @Transactional(readOnly = true)
    public ReceiverVO getDefaultReceiver(String customerId) {
        KryAssert.notEmpty(customerId, "customerId must be provided.");

        Customer customer = customerRepository.load(customerId);
        Receiver receiver = customer.defaultReceiver();

        if (receiver != null) {
            return populateReceiverVO(receiver);
        }

        return null;
    }

    private ReceiverVO populateReceiverVO(Receiver receiver) {
        ReceiverVO receiverVO = new ReceiverVO();
        receiverVO.setReceiverId(receiver.getId()).setReceiverName(receiver.getName());

        receiverVO.setBuildingAndNumber(receiver.getAddress().getBuildingAndNumber())
                .setCity(receiver.getAddress().getCity()).setDistrict(receiver.getAddress().getDistrict())
                .setProvince(receiver.getAddress().getProvince()).setStreet(receiver.getAddress().getStreet());

        receiverVO.setReceiverMobile(receiver.getContact().getMobile());
        return receiverVO;
    }

}
