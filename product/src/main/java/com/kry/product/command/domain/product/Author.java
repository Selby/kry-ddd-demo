package com.kry.product.command.domain.product;

import com.kry.domain.ValueObject;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Author implements ValueObject {

    private String name;
    private String profile;

}
