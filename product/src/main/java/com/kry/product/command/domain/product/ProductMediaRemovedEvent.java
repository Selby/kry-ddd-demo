package com.kry.product.command.domain.product;

import com.kry.domain.BaseDomainEvent;
import lombok.Getter;

import java.util.List;

@Getter
public final class ProductMediaRemovedEvent extends BaseDomainEvent {

    private List<MediaId> mediaIds;

    public ProductMediaRemovedEvent(List<MediaId> mediaIds) {
        this.mediaIds = mediaIds;
    }
}
