package com.kry.product.command.domain.product;

import com.google.common.collect.Lists;
import com.kry.domain.Aggregate;
import com.kry.product.command.application.valueobjects.AuthorPublishingVO;
import lombok.Builder;
import lombok.Getter;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Builder
@Getter
public class Product extends Aggregate {

    private String id;
    private String name;
    private String briefDesc;
    private Author author;
    private Publishing publishing;
    private BigDecimal price;
    private ProductStatus status;

    private List<MediaId> medias;

    private List<CategoryId> categories;

    public void create() {
        id = UUID.randomUUID().toString();
        status = ProductStatus.NON_SELLING;
        applyEvent(new ProductCreatedEvent(id, name));
    }

    public void changeName(String productName) {
        name = productName;
    }

    public void changePrice(BigDecimal newPrice) {
        price = newPrice;
    }

    public void sell() {
        status = ProductStatus.SELLING;
    }

    public void unsell() {
        status = ProductStatus.NON_SELLING;
    }

    public void changeAuthorAndPublishingInfo(AuthorPublishingVO authorPublishingVO) {
        author = Author.builder()
                .name(authorPublishingVO.getAuthorName())
                .profile(authorPublishingVO.getAuthorProfile())
                .build();
        publishing = Publishing.builder()
                .company(authorPublishingVO.getPublishingName())
                .date(authorPublishingVO.getPublishedDate())
                .build();
    }

    public void assignToCategories(List<String> categoryIds) {
        if (CollectionUtils.isEmpty(categoryIds)) {
            categories = Lists.newArrayList();
        } else {
            List<CategoryId> newCategories = Lists.newArrayList();
            for (String id : categoryIds) {
                CategoryId categoryId = new CategoryId(id);
                newCategories.add(categoryId);
            }
            categories = newCategories;
        }
    }

    public void maintainMedia(List<String> mediaIds) {
        List<MediaId> newMedias = Lists.newArrayList();
        List<MediaId> mediasRemoved = Lists.newArrayList();
        if (CollectionUtils.isEmpty(mediaIds)) {
            mediasRemoved.addAll(medias);
        } else {
            for (String id : mediaIds) {
                MediaId mediaId = new MediaId(id);
                newMedias.add(mediaId);
                if (!medias.contains(mediaId)) {
                    mediasRemoved.add(mediaId);
                }
            }
        }
        if (!CollectionUtils.isEmpty(mediasRemoved)) {
            applyEvent(new ProductMediaRemovedEvent(mediasRemoved));
        }
        medias = newMedias;
    }

    public void addMedias(List<String> mediaIds) {
        if (CollectionUtils.isEmpty(medias)) {
            medias = Lists.newArrayList();
        }
        for (String id : mediaIds) {
            MediaId mediaId = new MediaId(id);
            if (!medias.contains(mediaId)) {
                medias.add(mediaId);
            }
        }
    }

    @Override
    public Serializable getAggregateId() {
        return id;
    }

}
