package com.kry.product.command.infra.repository.product.mybatis.mapper;

import com.kry.product.command.infra.repository.product.storageobjects.ProductSO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

@Mapper
public interface ProductMapper {

    @Insert({
            "insert into product (product_id, product_name, ",
            "brief_desc, price, ",
            "status, author_name, ",
            "author_profile, publishing_name, ",
            "publishing_date, medias, ",
            "create_user, create_date, ",
            "update_user, update_date, ",
            "ver)",
            "values (#{productId,jdbcType=VARCHAR}, #{productName,jdbcType=VARCHAR}, ",
            "#{briefDesc,jdbcType=VARCHAR}, #{price,jdbcType=DECIMAL}, ",
            "#{status,jdbcType=TINYINT}, #{authorName,jdbcType=VARCHAR}, ",
            "#{authorProfile,jdbcType=VARCHAR}, #{publishingName,jdbcType=VARCHAR}, ",
            "#{publishedDate,jdbcType=DATE}, #{medias,jdbcType=VARCHAR}, ",
            "#{createUser,jdbcType=VARCHAR}, #{createDate,jdbcType=TIMESTAMP}, ",
            "#{updateUser,jdbcType=VARCHAR}, #{updateDate,jdbcType=TIMESTAMP}, ",
            "#{ver,jdbcType=TINYINT})"
    })
    int insert(ProductSO record);

    @Select({
            "select",
            "product_id, product_name, brief_desc, price, status, author_name, author_profile, ",
            "publishing_name, publishing_date, medias, create_user, create_date, update_user, ",
            "update_date, ver",
            "from product",
            "where product_id = #{productId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
            @Result(column="brief_desc", property="briefDesc", jdbcType=JdbcType.VARCHAR),
            @Result(column="price", property="price", jdbcType=JdbcType.DECIMAL),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="author_name", property="authorName", jdbcType=JdbcType.VARCHAR),
            @Result(column="author_profile", property="authorProfile", jdbcType=JdbcType.VARCHAR),
            @Result(column="publishing_name", property="publishingName", jdbcType=JdbcType.VARCHAR),
            @Result(column="publishing_date", property="publishedDate", jdbcType=JdbcType.DATE),
            @Result(column="medias", property="medias", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    ProductSO selectByPrimaryKey(String productId);

    @Update({
            "update product",
            "set product_name = #{entity.productName,jdbcType=VARCHAR},",
            "brief_desc = #{entity.briefDesc,jdbcType=VARCHAR},",
            "price = #{entity.price,jdbcType=DECIMAL},",
            "status = #{entity.status,jdbcType=TINYINT},",
            "author_name = #{entity.authorName,jdbcType=VARCHAR},",
            "author_profile = #{entity.authorProfile,jdbcType=VARCHAR},",
            "publishing_name = #{entity.publishingName,jdbcType=VARCHAR},",
            "publishing_date = #{entity.publishedDate,jdbcType=DATE},",
            "medias = #{entity.medias,jdbcType=VARCHAR},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where product_id = #{entity.productId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);
}
