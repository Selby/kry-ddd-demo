package com.kry.product.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Data
public class CategoryVO {

    private String categoryId;
    private String name;
    private String description;
    private String status;

}
