package com.kry.product.command.infra.repository.product;

import com.google.common.collect.Lists;
import com.kry.domain.AbstractRepository;
import com.kry.domain.DomainEvent;
import com.kry.exception.AggregateNotFoundException;
import com.kry.infra.repository.DaoProvider;
import com.kry.product.command.domain.product.*;
import com.kry.product.command.infra.repository.product.mybatis.mapper.ProductCategoryMapper;
import com.kry.product.command.infra.repository.product.storageobjects.ProductCategorySO;
import com.kry.product.command.infra.repository.product.storageobjects.ProductSO;
import com.kry.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.List;

@Repository
public class ProductRepositoryImpl extends AbstractRepository<Product> implements ProductRepository {

    @Autowired
    private DaoProvider daoProvider;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Override
    protected void saveAggregate(Product aggregate) {
        ProductSO productSO = new ProductSO();
        productSO.setProductId(aggregate.getId()).setProductName(aggregate.getName())
                .setBriefDesc(aggregate.getBriefDesc()).setPrice(aggregate.getPrice())
                .setStatus(aggregate.getStatus().value());

        if (aggregate.getAuthor() != null) {
            productSO.setAuthorName(aggregate.getAuthor().getName())
                    .setAuthorProfile(aggregate.getAuthor().getProfile());
        }

        if (aggregate.getPublishing() != null) {
            productSO.setPublishedDate(aggregate.getPublishing().getDate())
                    .setPublishingName(aggregate.getPublishing().getCompany());
        }

        if (!CollectionUtils.isEmpty(aggregate.getMedias())) {
            String mediaIds = "";
            for (MediaId id : aggregate.getMedias()) {
                mediaIds = mediaIds.concat(id.getFileId()).concat(";");
            }
            productSO.setMedias(mediaIds.substring(0, mediaIds.length() - 1));
        }

        daoProvider.save(productSO);

        saveRelationshipBetweenProductAndCategory(aggregate);
    }

    private void saveRelationshipBetweenProductAndCategory(Product aggregate) {
        productCategoryMapper.deleteByProductId(aggregate.getId());
        List<CategoryId> categoryIds = aggregate.getCategories();
        if (!CollectionUtils.isEmpty(categoryIds)) {
            for (CategoryId id : aggregate.getCategories()) {
                productCategoryMapper.insert(new ProductCategorySO(aggregate.getId(), id.getId()));
            }
        }
    }

    @Override
    protected void publishEvent(Product aggregate) {
        DomainEvent event = getEvent(aggregate);
        if (event != null) {
            kafkaTemplate.send(getDefaultTopicName(event), aggregate.getEntityId().toString(), JsonUtils.toJson(event));
        }
    }

    @Override
    public Product load(Serializable aggregateId) {
        ProductSO productSO = daoProvider.loadOne(ProductSO.class, aggregateId);
        if (productSO == null) {
            throw new AggregateNotFoundException();
        }

        List<MediaId> medias = Lists.newArrayList();
        if (!StringUtils.isEmpty(productSO.getMedias())) {
            String[] ids = productSO.getMedias().split(";");
            for (String id : ids) {
                medias.add(new MediaId(id));
            }
        }

        List<CategoryId> categories = Lists.newArrayList();
        List<ProductCategorySO> list = productCategoryMapper.selectByProductId(productSO.getProductId());
        if (!CollectionUtils.isEmpty(list)) {
            for (ProductCategorySO so : list) {
                categories.add(new CategoryId(so.getCategoryId()));
            }
        }

        Product product = Product.builder().id(productSO.getProductId()).name(productSO.getProductName())
                .price(productSO.getPrice()).briefDesc(productSO.getBriefDesc())
                .author(Author.builder().name(productSO.getAuthorName())
                        .profile(productSO.getAuthorProfile()).build())
                .publishing(Publishing.builder().company(productSO.getPublishingName())
                        .date(productSO.getPublishedDate()).build())
                .status(ProductStatus.from(productSO.getStatus()))
                .medias(medias)
                .categories(categories)
                .build();

        return product;
    }

}
