package com.kry.product.command.domain.product;

import com.kry.domain.ValueObject;
import lombok.Getter;

import java.util.Objects;

@Getter
public class CategoryId implements ValueObject {

    private String id;

    public CategoryId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryId that = (CategoryId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
