package com.kry.product.command.domain.category;

import com.kry.domain.Repository;

public interface CategoryRepository extends Repository<Category> {
}
