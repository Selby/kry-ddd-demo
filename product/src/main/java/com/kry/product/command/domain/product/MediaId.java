package com.kry.product.command.domain.product;

import com.kry.domain.ValueObject;
import lombok.Getter;

import java.util.Objects;

@Getter
public class MediaId implements ValueObject {

    private String fileId;

    public MediaId(String fileId) {
        this.fileId = fileId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaId mediaId = (MediaId) o;
        return Objects.equals(fileId, mediaId.fileId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(fileId);
    }
}
