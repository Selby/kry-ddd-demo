package com.kry.product.command.adapter.web.common;

import com.kry.adapter.web.Response;
import com.kry.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Slf4j
@ControllerAdvice
public class ExceptionAdvice {

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Response wrapException(Exception e) {
        Response response;
        if (e instanceof BusinessException) {
            BusinessException exception = (BusinessException)e;
            response = Response.newWithFailed(exception.getErrorCode(), exception.getErrorMessage());
        } else if (e instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException exception = (MethodArgumentNotValidException)e;
            List<ObjectError> errorList = exception.getBindingResult().getAllErrors();
            String errorMsg = "";
            for (ObjectError error: errorList) {
                errorMsg = errorMsg.concat(error.getDefaultMessage()).concat("; ");
            }
            errorMsg = errorMsg.substring(0, errorMsg.length() - 2);
            response = Response.newWithFailed(null, errorMsg);
            log.error(errorMsg, e);
        } else {
            response = Response.newWithFailedByDefault();
            log.error(e.getMessage(), e);
        }
        return response;
    }

}
