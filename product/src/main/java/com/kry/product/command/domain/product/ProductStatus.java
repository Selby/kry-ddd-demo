package com.kry.product.command.domain.product;

import com.kry.exception.UnrecognizedException;

public enum  ProductStatus {

    SELLING(1), NON_SELLING(2);

    private int status;
    ProductStatus(int status) {
        this.status = status;
    }

    public static ProductStatus from(int value) {
        if (value == 1) {
            return SELLING;
        } else if (value == 2) {
            return NON_SELLING;
        } else {
            throw new UnrecognizedException();
        }
    }

    public int value() {
        return status;
    }

    @Override
    public String toString() {
        return String.valueOf(status);
    }
}
