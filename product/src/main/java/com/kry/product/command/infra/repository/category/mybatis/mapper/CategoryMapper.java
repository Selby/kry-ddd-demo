package com.kry.product.command.infra.repository.category.mybatis.mapper;

import com.kry.product.command.infra.repository.category.storageobjects.CategorySO;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;

import java.util.Map;

@Mapper
public interface CategoryMapper {

    @Insert({
            "insert into category (category_id, category_name, ",
            "description, status, ",
            "create_user, create_date, ",
            "update_user, update_date, ",
            "ver)",
            "values (#{categoryId,jdbcType=VARCHAR}, #{categoryName,jdbcType=VARCHAR}, ",
            "#{description,jdbcType=VARCHAR}, #{status,jdbcType=TINYINT}, ",
            "#{createUser,jdbcType=VARCHAR}, #{createDate,jdbcType=TIMESTAMP}, ",
            "#{updateUser,jdbcType=VARCHAR}, #{updateDate,jdbcType=TIMESTAMP}, ",
            "#{ver,jdbcType=TINYINT})"
    })
    int insert(CategorySO record);

    @Select({
            "select",
            "category_id, category_name, description, status, create_user, create_date, update_user, ",
            "update_date, ver",
            "from category",
            "where category_id = #{categoryId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column="category_id", property="categoryId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="category_name", property="categoryName", jdbcType=JdbcType.VARCHAR),
            @Result(column="description", property="description", jdbcType=JdbcType.VARCHAR),
            @Result(column="status", property="status", jdbcType=JdbcType.TINYINT),
            @Result(column="create_user", property="createUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="create_date", property="createDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="update_user", property="updateUser", jdbcType=JdbcType.VARCHAR),
            @Result(column="update_date", property="updateDate", jdbcType=JdbcType.TIMESTAMP),
            @Result(column="ver", property="ver", jdbcType=JdbcType.TINYINT)
    })
    CategorySO selectByPrimaryKey(String categoryId);

    @Update({
            "update category",
            "set category_name = #{entity.categoryName,jdbcType=VARCHAR},",
            "description = #{entity.description,jdbcType=VARCHAR},",
            "status = #{entity.status,jdbcType=TINYINT},",
            "create_user = #{entity.createUser,jdbcType=VARCHAR},",
            "create_date = #{entity.createDate,jdbcType=TIMESTAMP},",
            "update_user = #{entity.updateUser,jdbcType=VARCHAR},",
            "update_date = #{entity.updateDate,jdbcType=TIMESTAMP},",
            "ver = #{entity.ver,jdbcType=TINYINT}",
            "where category_id = #{entity.categoryId,jdbcType=VARCHAR} and ver = #{ver,jdbcType=TINYINT}"
    })
    int updateByPrimaryKey(Map<String, Object> params);

}
