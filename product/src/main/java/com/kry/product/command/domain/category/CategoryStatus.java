package com.kry.product.command.domain.category;

import com.kry.exception.UnrecognizedException;

public enum CategoryStatus {

    ENABLED(1), DISABLED(2);

    private int status;
    CategoryStatus(int status) {
        this.status = status;
    }

    public static CategoryStatus from(int value) {
        if (value == 1) {
            return ENABLED;
        } else if (value == 2) {
            return DISABLED;
        } else {
            throw new UnrecognizedException();
        }
    }

    public int value() {
        return this.status;
    }

    @Override
    public String toString() {
        return String.valueOf(status);
    }
}
