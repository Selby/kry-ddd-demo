package com.kry.product.command.domain.product;

import com.kry.domain.ValueObject;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;

@Builder
@Getter
public class Publishing implements ValueObject {

    private String company;
    private Date date;
}
