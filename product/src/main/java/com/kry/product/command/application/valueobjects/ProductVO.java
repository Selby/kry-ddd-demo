package com.kry.product.command.application.valueobjects;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Accessors(chain = true)
@Data
public class ProductVO {

    private String productId;
    private String name;
    private String briefDesc;
    private BigDecimal price;

    private String status;

    private String authorName;
    private String authorProfile;

    private String publishingName;
    private Date publishedDate;

    private List<String> mediaIds;
    private List<String> categoryIds;
}
