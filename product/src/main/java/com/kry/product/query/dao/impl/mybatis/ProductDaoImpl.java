package com.kry.product.query.dao.impl.mybatis;

import com.google.common.collect.Lists;
import com.kry.product.query.dao.ProductDAO;
import com.kry.product.query.dao.impl.mybatis.mapper.ProductQueryMapper;
import com.kry.product.query.dataobjects.CategoryDO;
import com.kry.product.query.dataobjects.ProductDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDAO {

    @Autowired
    private ProductQueryMapper queryMapper;


    @Override
    public List<CategoryDO> findAllAvailableCategories() {
        return queryMapper.findAllAvailableCategories();
    }

    @Override
    public List<ProductDO> findSellingProductsByName(String productName) {
        if (StringUtils.isEmpty(productName) || productName.length() < 3) {
            return Lists.newArrayList();
        }
        String queryStr = productName.trim().concat("%");
        List<ProductDO> result = queryMapper.findSellingProductsByName(queryStr);

        populateMedia(result);

        return result;
    }

    private void populateMedia(List<ProductDO> result) {
        for (ProductDO product: result) {
            if (StringUtils.isEmpty(product.getMedias())) {
                product.setMediaList(Lists.newArrayList());
            } else {
                product.setMediaList(Arrays.asList(product.getMedias().split(";")));
            }
        }
    }

    @Override
    public List<ProductDO> findSellingProductsByCategory(String categoryId) {
        List<ProductDO> result = queryMapper.findSellingProductsByCategory(categoryId);
        populateMedia(result);
        return result;
    }
}
