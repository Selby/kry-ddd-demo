package com.kry.product.query.controller;


import com.kry.product.query.dao.ProductDAO;
import com.kry.product.query.dataobjects.CategoryDO;
import com.kry.product.query.dataobjects.ProductDO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Catalog Queries", tags = "200 - Catalog Query APIs")
@RestController
@RequestMapping("/queries/catalog")
public class ProductQueryController {


    @Autowired
    private ProductDAO productDAO;

    @ApiOperation(value = "Find products according to product name, support fuzzy query.", notes = "Find products according to product name, support fuzzy query.")
    @ResponseBody
    @GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ProductDO> queryByName(@RequestParam String productName) {
        return productDAO.findSellingProductsByName(productName);
    }


    @ApiOperation(value = "Find products according to category.", notes = "Find products according to category.")
    @ResponseBody
    @GetMapping(value = "/{categoryId}/products", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ProductDO> queryByCategory(@PathVariable String categoryId) {
        return productDAO.findSellingProductsByCategory(categoryId);
    }


    @ApiOperation(value = "Find all available categories.", notes = "Find all available categories.")
    @ResponseBody
    @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<CategoryDO> getAllCategories() {
        return productDAO.findAllAvailableCategories();
    }

}
