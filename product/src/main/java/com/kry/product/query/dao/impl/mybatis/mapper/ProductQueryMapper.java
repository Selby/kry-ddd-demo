package com.kry.product.query.dao.impl.mybatis.mapper;

import com.kry.product.query.dataobjects.CategoryDO;
import com.kry.product.query.dataobjects.ProductDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import java.util.List;

@Mapper
public interface ProductQueryMapper {


    @Select({
            "select",
            "category_id, category_name",
            "from category",
            "where status = 1"
    })
    @Results({
            @Result(column="category_id", property="categoryId", jdbcType=JdbcType.VARCHAR),
            @Result(column="category_name", property="categoryName", jdbcType=JdbcType.VARCHAR)
    })
    List<CategoryDO> findAllAvailableCategories();


    @Select({
            "select",
            "product_id, product_name, brief_desc, price, author_name, author_profile, ",
            "publishing_name, publishing_date, medias ",
            "from product",
            "where product_name like #{productName,jdbcType=VARCHAR} and status = 1"
    })
    @Results({
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
            @Result(column="brief_desc", property="briefDesc", jdbcType=JdbcType.VARCHAR),
            @Result(column="price", property="price", jdbcType=JdbcType.DECIMAL),
            @Result(column="author_name", property="authorName", jdbcType=JdbcType.VARCHAR),
            @Result(column="author_profile", property="authorProfile", jdbcType=JdbcType.VARCHAR),
            @Result(column="publishing_name", property="publishingName", jdbcType=JdbcType.VARCHAR),
            @Result(column="publishing_date", property="publishedDate", jdbcType=JdbcType.DATE),
            @Result(column="medias", property="medias", jdbcType=JdbcType.VARCHAR)
    })
    List<ProductDO> findSellingProductsByName(String productName);


    @Select({
            "select",
            "t.product_id, product_name, brief_desc, price, author_name, author_profile, ",
            "publishing_name, publishing_date, medias ",
            "from product t join product_category p on t.product_id = p.product_id ",
            "where p.category_id = #{categoryId,jdbcType=VARCHAR} and t.status = 1"
    })
    @Results({
            @Result(column="product_id", property="productId", jdbcType=JdbcType.VARCHAR, id=true),
            @Result(column="product_name", property="productName", jdbcType=JdbcType.VARCHAR),
            @Result(column="brief_desc", property="briefDesc", jdbcType=JdbcType.VARCHAR),
            @Result(column="price", property="price", jdbcType=JdbcType.DECIMAL),
            @Result(column="author_name", property="authorName", jdbcType=JdbcType.VARCHAR),
            @Result(column="author_profile", property="authorProfile", jdbcType=JdbcType.VARCHAR),
            @Result(column="publishing_name", property="publishingName", jdbcType=JdbcType.VARCHAR),
            @Result(column="publishing_date", property="publishedDate", jdbcType=JdbcType.DATE),
            @Result(column="medias", property="medias", jdbcType=JdbcType.VARCHAR)
    })
    List<ProductDO> findSellingProductsByCategory(String categoryId);

}
