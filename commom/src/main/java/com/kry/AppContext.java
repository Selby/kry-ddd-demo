package com.kry;

/**
 * This is a global class. And it's safety at thread level.
 */
public final class AppContext {

    private AppContext(){}

    private static ThreadLocal<IUser> currentUser = new ThreadLocal<>();

    public static void setCurrentUser(IUser user) {
        currentUser.set(user);
    }

    public static IUser currentUser() {
        return currentUser.get();
    }
}
