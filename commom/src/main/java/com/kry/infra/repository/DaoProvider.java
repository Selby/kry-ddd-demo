package com.kry.infra.repository;

import java.io.Serializable;
import java.util.List;

public interface DaoProvider {

    <T extends BaseSO> Serializable save(T entity);

    <T extends BaseSO> T loadOne(Class<T> entityClass, Serializable entityId);

    <T extends BaseSO> List<T> loadList(Class<T> entityClass, Serializable entityId);

}
