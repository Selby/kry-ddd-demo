package com.kry.infra.repository.mybatis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface StorageMapper {

    Class value();

    /**
     * The method name of saving entity.
     * @return
     */
    String insertMethodName() default "insert";

    /**
     * The method name of updating entity according primary key.
     * @return
     */
    String updateMethodName() default "updateByPrimaryKey";

    /**
     * Specific a alias name for entity when updating.
     * @return
     */
    String aliasName() default "entity";

    /**
     * The method name of fetching a object according to primary key.
     * @return
     */
    String selectOneMethodName() default "selectByPrimaryKey";

    /**
     * The method name of fetching objects according to aggregate id.
     * @return
     */
    String selectListMethodName() default "selectByAggregateId";

}
