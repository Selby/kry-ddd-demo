package com.kry.utils;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public final class KryAssert extends Assert {

    public static void notEmpty(String object, String message) {
        if (StringUtils.isEmpty(object)) {
            throw new IllegalArgumentException(message);
        }
    }

}
