package com.kry.domain;

import java.util.Date;

/**
 * All domain events should implement it.
 * Suggestion all domain events implement it via inheriting BaseDomainEvent class.
 */
public interface DomainEvent {

    String getEventId();

    Date getOccurredDate();

}
