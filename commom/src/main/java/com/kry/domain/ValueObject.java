package com.kry.domain;

/**
 * This is a maker interface to indicate the object is a value object.
 */
public interface ValueObject {
}
