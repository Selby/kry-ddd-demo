package com.kry.exception;

public class EncryptException extends RuntimeException{

    public EncryptException(Throwable cause) {
        super(cause);
    }
}
