package com.kry;

import java.io.Serializable;

/**
 * User need to login has to implement this interface.
 */
public interface IUser {

    Serializable getUserId();

    String getUserName();

}
